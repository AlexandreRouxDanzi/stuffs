rm *.py*
echo "visit https://www.oneplus.com/fr/support/softwareupgrade/details?code=PM1596593883414 and download the firmware in the same directory as this script"
read -p "Press enter to continue"
wget https://raw.githubusercontent.com/cyxx/extract_android_ota_payload/master/extract_android_ota_payload.py
wget https://raw.githubusercontent.com/cyxx/extract_android_ota_payload/master/update_metadata_pb2.py

unzip Nord*.zip -d firmware
#rm Nord*.zip
mkdir payload_output
python3 extract_android_ota_payload.py firmware/payload.bin payload_output/
rm firmware -R
rsync -avr --remove-source-files --exclude='boot.img' --exclude='dtbo.img' --exclude='odm.img' --exclude='product.img' --exclude='recovery.img' --exclude='system.img' --exclude='system_ext.img' --exclude='vbmeta.img' --exclude='vbmeta_system.img'  --exclude='reserve.img'  --exclude='vendor.img' payload_output/* extracted/
rm payload_output -R
rm extracted/flashall_stock.sh
find extracted -iname '*.img' | while read file
do
NAME=`basename "$file"`
NAMENOEXT=${NAME%.???}


if cat getvarall | grep -q ${NAMENOEXT}_a
then
echo "fastboot flash "${NAMENOEXT}_a $NAME >> extracted/flashall_stock.sh
echo "fastboot flash "${NAMENOEXT}_b $NAME >> extracted/flashall_stock.sh
else
echo "fastboot flash "${NAMENOEXT} $NAME >> extracted/flashall_stock.sh
fi
done
cd extracted
zip ../extracted.zip *
cd ../
rm extracted -R
