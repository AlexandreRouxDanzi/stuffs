#!/bin/bash
packages="foundation.e.drive
foundation.e.accountmanager
com.android.webview
foundation.e.apps
foundation.e.blisslauncher
foundation.e.blissiconpack
foundation.e.browser
com.android.calculator2
foundation.e.calendar
com.android.deskclock
com.android.contacts
com.android.vending
com.android.documentsui
com.android.fmradio
com.android.gallery3d
org.documentfoundation.libreoffice
com.generalmagic.magicearth
foundation.e.mail
foundation.e.message
org.microg.gms.droidguard
com.google.android.gms
com.google.android.gsf
org.microg.nlp.backend.ichnaea
org.lineageos.eleven
org.microg.nlp.backend.nominatim
foundation.e.notes
foundation.e.camera
org.sufficientlysecure.keychain
org.lineageos.openweathermapprovider
com.gsnathan.pdfviewer
com.android.dialer
org.lineageos.recorder
com.android.settings
foundation.e.tasks
foundation.e.weather"


packagesInDevice=`adb shell pm list packages`
ko=""
while IFS= read -r line; do
	#test=`cat "$packagesInDevice" | grep "$line"`
    if [ `echo "$packagesInDevice" | grep "package:$line$"` ]
	then
	echo "$line ok"
	else
	echo "$line ko"
	ko=$ko" "$line
	fi
done <<< "$packages"
echo "ko packages are: "$ko
